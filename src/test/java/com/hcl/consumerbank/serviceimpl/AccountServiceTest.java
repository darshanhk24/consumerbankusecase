package com.hcl.consumerbank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.consumerbank.dto.AccountRequestDTO;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.repository.IAccountRepository;
import com.hcl.consumerbank.repository.ICustomerRepository;


@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

	@Mock
	IAccountRepository accountRepository;
	
	@Mock
	ICustomerRepository customerRepository;
	
	@InjectMocks
	AccountServiceImpl accountServiceImpl;
	
	AccountRequestDTO accountRequestDto;
	
	Account account1;
	Account account2;
	
	Customer customer1;
	
	List<Account> accountList = new ArrayList<Account>();
	
	
	@BeforeEach
	public void setUp()
	{
		accountRequestDto = new AccountRequestDTO();
		
		accountRequestDto.setAccountNumber(192929L);
		accountRequestDto.setAccountType("SB");
		accountRequestDto.setBalance(1000);
		accountRequestDto.setCustomerId(1);
		
		customer1 = new Customer();
		
		customer1.setCustomerId(1);
		customer1.setCustomerName("Arjesh");
		customer1.setPhoneNo("82662629992");
		customer1.setAddress("Mysore");
		
		account1 = new Account();
		account1.setAccountId(1);
		account1.setAccountNumber(192929L);
		account1.setAccountType("SB");
		account1.setBalance(1000);
		account1.setCustomer(customer1);
		
		account2 = new Account();
		account2.setAccountId(1);
		account2.setAccountNumber(192929L);
		account2.setAccountType("SB");
		account2.setBalance(1000);
		account2.setCustomer(customer1);
		
		accountList.add(account1);
		accountList.add(account2);
	}
	
	
	@Test
	@DisplayName("Save Account Data : Positive")
	public void saveAccountDataTest_Positive() throws IdNotFoundException
	{
		when(customerRepository.findById(1)).thenReturn(Optional.of(customer1));
		
		when(accountRepository.save(any(Account.class))).thenAnswer(i -> {
			Account account = i.getArgument(0);
			account.setAccountId(1);
			return account;
		});
		
		boolean result = accountServiceImpl.save(accountRequestDto);
		
		assertTrue(result);
		verify(accountRepository).save(any(Account.class));
	}
	
	
	@Test
	@DisplayName("Save Account Data : Negative")
	public void saveAccountDataTest_Negative() throws IdNotFoundException
	{
		
		when(customerRepository.findById(1)).thenReturn(Optional.of(customer1));
		
		when(accountRepository.save(any(Account.class))).thenReturn(null);
		
		
		boolean result = accountServiceImpl.save(accountRequestDto);
		
		assertFalse(result);
		verify(accountRepository).save(any(Account.class));

	}
	
	@Test
	@DisplayName("Display Account Data : Positive")
	public void displayAccountDataTest_Positive()
	{
		when(accountRepository.findAll()).thenReturn(accountList);
		
		
		List<AccountResponseDTO> accountResponseDTOList = accountServiceImpl.getAllAccountsData();
		
		assertNotNull(accountResponseDTOList);
		assertEquals(2, accountResponseDTOList.size());
	}
	
	@Test
	@DisplayName("Delete Account Data By ID : Positive")
	public void deleteAccountByIdTest_Positive() throws IdNotFoundException
	{
		when(accountRepository.findById(1)).thenReturn(Optional.of(account1));
		
		String result = accountServiceImpl.closeAccountById(account1.getAccountId());
		
		verify(accountRepository, times(1)).delete(account1);
		
		assertEquals("Successfully Deleted" , result);
	}
}

