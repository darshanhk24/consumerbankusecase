package com.hcl.consumerbank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.consumerbank.dto.CustomerRequestDTO;
import com.hcl.consumerbank.dto.CustomerResponse;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.repository.ICustomerRepository;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
	
	@Mock
	ICustomerRepository customerRepository;
	
	@InjectMocks
	CustomerServiceImpl customerServiceImpl;
	
	CustomerRequestDTO customerRequestDto;
	
	Customer customer1;
	
	Customer customer2;
	
	List<Customer> customerList = new ArrayList<Customer>();
	List<CustomerResponse> customerResponseList = new ArrayList<>();
	
	ProjectionFactory factory = new SpelAwareProxyProjectionFactory();
	
	String customerName = "Arj";
	
	
	@BeforeEach
	public void setUp()
	{
		customerRequestDto = new CustomerRequestDTO();
		
		customerRequestDto.setCustomerName("Arjun");
		customerRequestDto.setPhoneNo("64667829992");
		customerRequestDto.setAddress("Bangalore");
		
		customer1 = new Customer();
		
		customer1.setCustomerId(1);
		customer1.setCustomerName("Arjesh");
		customer1.setPhoneNo("82662629992");
		customer1.setAddress("Mysore");
		
		customer2 = new Customer();
		
		customer2.setCustomerId(2);
		customer2.setCustomerName("Arjun");
		customer2.setPhoneNo("64667829992");
		customer2.setAddress("Bangalore");
			
		customerList.add(customer1);
		customerList.add(customer2);
		
		var customerResponse1 = factory.createProjection(CustomerResponse.class);
		customerResponse1.setCustomerName("Arjun");
		customerResponse1.setPhoneNo("74647488484");
		customerResponse1.setAddress("Bangalore");
		
		var customerResponse2 = factory.createProjection(CustomerResponse.class);
		customerResponse2.setCustomerName("Arjun A H");
		customerResponse2.setPhoneNo("876438484");
		customerResponse2.setAddress("Mysore");
		
		customerResponseList.add(customerResponse1);
		customerResponseList.add(customerResponse2);
		
	}
	
	@Test
	@DisplayName("Save Customer Data : Positive")
	public void saveCustomerDataTest_Positive()
	{
		when(customerRepository.save(any(Customer.class))).thenAnswer(i -> {
			Customer customer = i.getArgument(0);
			customer.setCustomerId(1);
			return customer;
		});
		
		boolean result = customerServiceImpl.save(customerRequestDto);
		
		assertTrue(result);
		verify(customerRepository).save(any(Customer.class));
		
	}
	
	@Test
	@DisplayName("Save Customer Data : Negative")
	public void saveCustomerDataTest_Negative()
	{
		when(customerRepository.save(any(Customer.class))).thenReturn(null);
		
		
		boolean result = customerServiceImpl.save(customerRequestDto);
		
		assertFalse(result);
		verify(customerRepository).save(any(Customer.class));
	}
	
	
	@Test
	@DisplayName("Display Customer Data : Positive")
	public void displayCustomerDataTest_Positive()
	{
		when(customerRepository.findAll()).thenReturn(customerList);
		
		
		List<CustomerResponseDTO> customerResponseDTOList = customerServiceImpl.getAllCustomers();
		
		assertNotNull(customerResponseDTOList);
		assertEquals(2, customerResponseDTOList.size());
	}
	
	
	@Test
	@DisplayName("Display Customer Data By Name : Positive")
	public void displayCustomerByNameTest_Positive()
	{
		when(customerRepository.findByCustomerNameContaining(customerName)).thenReturn(customerResponseList);
		
		
		List<CustomerResponse> customerResponseList = customerServiceImpl.getCustomerByName(customerName);
		
		assertNotNull(customerResponseList);
		assertEquals(2, customerResponseList.size());
	}
	
	
	@DisplayName("Display Customer Data By ID : Positive")
	public void displayCustomerByIdTest_Positive() throws IdNotFoundException
	{
		when(customerRepository.findById(1)).thenReturn(Optional.of(customer1));
		
		
		CustomerResponseDTO customerResponseDto = customerServiceImpl.getCustomerById(1);
		
		assertNotNull(customerResponseDto);
		assertEquals("Arjesh" , customerResponseDto.getCustomerName());
	}
	
	@Test
	@DisplayName("Display Customer Data By ID : Negative")
	public void displayCustomerByIdTest_Negative() throws IdNotFoundException
	{
		when(customerRepository.findById(1)).thenReturn(null);
		
		
		NullPointerException e = assertThrows(NullPointerException.class, () ->  customerServiceImpl.getCustomerById(1));
		
	}
	
	@Test
	@DisplayName("Delete Customer Data By ID : Positive")
	public void deleteCustomerByIdTest_Positive() throws IdNotFoundException
	{

		when(customerRepository.findById(1)).thenReturn(Optional.of(customer1));
		
		String result = customerServiceImpl.deleteCustomerById(customer1.getCustomerId());
		
		verify(customerRepository, times(1)).delete(customer1);
		
		assertEquals("Successfully Deleted" , result);
	}
	
	
	
	@Test
	@DisplayName("Update Customer Data By ID : Positive")
	public void updateCustomerByIdTest_Positive() throws IdNotFoundException
	{
		
		when(customerRepository.findById(1)).thenReturn(Optional.of(customer1));
		
		
		when(customerRepository.save(any(Customer.class))).thenAnswer(i -> {
			Customer customer = i.getArgument(0);
			customer.setCustomerId(1);
			customer.setCustomerName("Darshan H K");
			return customer;
		});
		
		String result = customerServiceImpl.updateCustomer(customerRequestDto, 1);
		
		assertEquals("Darshan H K", customer1.getCustomerName());
		assertEquals("Customer updated successfully" , result);
	}
	
	
	
	


}
