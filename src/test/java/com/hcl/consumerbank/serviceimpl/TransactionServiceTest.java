package com.hcl.consumerbank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import com.hcl.consumerbank.dto.TransactionRequestDTO;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.entity.Transaction;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.exception.InsufficientBalanceException;
import com.hcl.consumerbank.repository.IAccountRepository;
import com.hcl.consumerbank.repository.ITransactionRepository;


@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

	@Mock
	ITransactionRepository transactionRepository;
	
	@Mock
	IAccountRepository accountRepository;
	
	
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	
	TransactionRequestDTO transactionRequestDto1;
	TransactionRequestDTO transactionRequestDto2;
	
	TransactionResponseDTO transactionResponseDto1;
	TransactionResponseDTO transactionResponseDto2;
	
	Transaction transaction1;
	Transaction transaction2;
	
	Account account;
	
	Customer customer;
	
	
	
	List<Transaction> transactionList = new ArrayList<>();
	
	
	@BeforeEach
	public void setUp()
	{
		transactionRequestDto1 = new TransactionRequestDTO();
		
		transactionRequestDto1.setAccountId(1);
		transactionRequestDto1.setAmount(500);
		
		customer = new Customer();
		
		customer.setCustomerId(1);
		customer.setCustomerName("Arjesh");
		customer.setPhoneNo("82662629992");
		customer.setAddress("Mysore");
		
		account = new Account();
		account.setAccountId(1);
		account.setAccountNumber(192929L);
		account.setAccountType("SB");
		account.setBalance(1000);
		account.setCustomer(customer);
		
		
		
		transaction1 = new Transaction();
		transaction1.setTransactionId(1);
		
		transaction1.setTransactionDate(Date.valueOf(LocalDate.now()));
		transaction1.setTransactionType("DEBIT");
		transaction1.setAmount(1000);
		transaction1.setTransactionNumber("1DQTSGSTST653");
		transaction1.setAccount(account);
		
		
		transaction2 = new Transaction();
		
		transaction2.setTransactionDate(Date.valueOf(LocalDate.now()));
		transaction2.setTransactionType("DEBIT");
		transaction2.setAmount(1000);
		transaction2.setTransactionNumber("1DQTSGSTST653");
		transaction2.setAccount(account);
		
		transactionList.add(transaction1);
		transactionList.add(transaction2);
		
		
	}
	
	@Test
	@DisplayName("Save Transaction : Positive")
	public void saveTransactionDataTest_Positive() throws IdNotFoundException, InsufficientBalanceException
	{
		
		
		when(accountRepository.findById(1)).thenReturn(Optional.of(account));
		
		when(transactionRepository.save(any(Transaction.class))).thenAnswer(i -> {
			Transaction transaction = i.getArgument(0);
			transaction.setTransactionId(1);
			return transaction;
		});
		
		
		List<TransactionResponseDTO> transactionResponseList = transactionServiceImpl.save(transactionRequestDto1, 1);
		
		assertNotNull(transactionResponseList);
		assertEquals(2, transactionResponseList.size());
	}
	
	@Test
	@DisplayName("Display All Transactions : Positive")
	public void displayAllTransactionsDataTest_Positive()
	{
		when(transactionRepository.findAll()).thenReturn(transactionList);
		
		
		List<TransactionResponseDTO> transactionResponseList = transactionServiceImpl.getAllTransaction();
		
		assertNotNull(transactionResponseList);
		assertEquals(2, transactionResponseList.size());
	}
	
}
