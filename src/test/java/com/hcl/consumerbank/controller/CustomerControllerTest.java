package com.hcl.consumerbank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.consumerbank.dto.CustomerRequestDTO;
import com.hcl.consumerbank.dto.CustomerResponse;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.service.ICustomerService;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {
	
	@Mock
	ICustomerService customerService;
	
	@InjectMocks
	CustomerController customerController;
	
	CustomerRequestDTO customerRequestDto1;
	CustomerRequestDTO customerRequestDto2;
	
	CustomerResponseDTO customerResponseDto1;
	CustomerResponseDTO customerResponseDto2;
	
	
	
	List<CustomerResponseDTO> customerResponseDTOList = new ArrayList<>();
	List<CustomerResponse> customerResponseList = new ArrayList<>();
	
	String customerName = "Arjun";
	String PhoneNo = "646738893";
	String address = "Bangalore";
	
	ProjectionFactory factory = new SpelAwareProxyProjectionFactory();
	
	
	
	
	@BeforeEach
	public void setUp()
	{
		customerRequestDto1 = new CustomerRequestDTO();
		
		customerRequestDto1.setCustomerName("Arjun");
		customerRequestDto1.setPhoneNo("64667829992");
		customerRequestDto1.setAddress("Bangalore");
		
		customerResponseDto1 = new CustomerResponseDTO();
		
		BeanUtils.copyProperties(customerRequestDto1, customerResponseDto1);
		
		customerResponseDto2 = new CustomerResponseDTO();
		
		customerResponseDto2.setCustomerName("Anan");
		customerResponseDto2.setPhoneNo("6477388363");
		customerResponseDto2.setAddress("Mysore");
		
		customerResponseDTOList.add(customerResponseDto1);
		customerResponseDTOList.add(customerResponseDto2);
		
		
		var customerResponse1 = factory.createProjection(CustomerResponse.class);
		customerResponse1.setCustomerName("Arjun");
		customerResponse1.setPhoneNo("74647488484");
		customerResponse1.setAddress("Bangalore");
		
		var customerResponse2 = factory.createProjection(CustomerResponse.class);
		customerResponse2.setCustomerName("Arjun A H");
		customerResponse2.setPhoneNo("876438484");
		customerResponse2.setAddress("Mysore");
		
		customerResponseList.add(customerResponse1);
		customerResponseList.add(customerResponse2);
		
		
	}
	
	
	@Test
	@DisplayName("Save Customer Data : Positive")
	public void saveCustomerDataTest_Positive()
	{
		when(customerService.save(customerRequestDto1)).thenReturn(true);
		
		
		ResponseEntity<String> result = customerController.saveCustomerData(customerRequestDto1);
		
		assertEquals("Customer saved successfully", result.getBody());
		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
	}
	
	
	@Test
	@DisplayName("Save Customer Data : Negative")
	public void saveCustomerDataTest_Negative()
	{
		when(customerService.save(customerRequestDto1)).thenReturn(false);
		
		
		ResponseEntity<String> result = customerController.saveCustomerData(customerRequestDto1);
		
		assertEquals("Customer not saved", result.getBody());
		assertEquals(HttpStatus.NOT_ACCEPTABLE, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Display Customer Data : Positive")
	public void displayCustomerDataTest_Positive()
	{
		when(customerService.getAllCustomers()).thenReturn(customerResponseDTOList);
		
		
		ResponseEntity<List<CustomerResponseDTO>> result = customerController.getAllCustomerData();
		
		assertNotNull(customerResponseDTOList);
		assertEquals(2, customerResponseDTOList.size());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Display Customer Data By Name : Positive")
	public void displayCustomerByNameTest_Positive()
	{
		when(customerService.getCustomerByName(customerName)).thenReturn(customerResponseList);
		
		
		ResponseEntity<List<CustomerResponse>> result = customerController.getCustomerByName(customerName);
		
		assertNotNull(customerResponseList);
		assertEquals(2, customerResponseList.size());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Display Customer Data By ID : Positive")
	public void displayCustomerByIdTest_Positive() throws IdNotFoundException
	{
		when(customerService.getCustomerById(1)).thenReturn(customerResponseDto1);
		
		
		ResponseEntity<CustomerResponseDTO> result = customerController.getCustomerById(1);
		
		assertNotNull(result);
		assertEquals("Arjun" , customerResponseDto1.getCustomerName());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Delete Customer Data By ID : Positive")
	public void deleteCustomerByIdTest_Positive() throws IdNotFoundException
	{
		when(customerService.deleteCustomerById(1)).thenReturn("Successfully Deleted");
		
		ResponseEntity<String> result = customerController.deleteCustomerById(1);
		
		assertEquals("Successfully Deleted" , result.getBody());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Update Customer Data By ID : Positive")
	public void updateCustomerByIdTest_Positive() throws IdNotFoundException
	{
		customerRequestDto1.setCustomerName("Darshan H K");
		
		when(customerService.updateCustomer(customerRequestDto1, 1)).thenReturn("Customer updated successfully");
		
		ResponseEntity<String> result = customerController.updateCustomerById(customerRequestDto1, 1);
		
		assertEquals("Darshan H K", customerRequestDto1.getCustomerName());
		assertEquals("Customer updated successfully" , result.getBody());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}
