package com.hcl.consumerbank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.consumerbank.dto.AccountRequestDTO;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.dto.TransactionRequestDTO;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.exception.InsufficientBalanceException;
import com.hcl.consumerbank.service.IAccountService;
import com.hcl.consumerbank.service.ITransactionService;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {
	
	@Mock
	ITransactionService transactionService;
	
	
	@InjectMocks
	TransactionController transactionController;
	
	TransactionRequestDTO transactionRequestDto1;
	TransactionRequestDTO transactionRequestDto2;
	
	TransactionResponseDTO transactionResponseDto1;
	TransactionResponseDTO transactionResponseDto2;
	
	
	
	List<TransactionResponseDTO> transactionResponseDTOList = new ArrayList<>();
	
	
	@BeforeEach
	public void setUp()
	{
		transactionRequestDto1 = new TransactionRequestDTO();
		
		transactionRequestDto1.setAccountId(1);
		transactionRequestDto1.setAmount(500);
		
		transactionResponseDto1 = new TransactionResponseDTO();
		
		transactionResponseDto1.setTransactionDate(Date.valueOf(LocalDate.now()));
		transactionResponseDto1.setTransactionType("DEBIT");
		transactionResponseDto1.setAmount(1000);
		transactionResponseDto1.setTransactionNumber("1DQTSGSTST653");
		transactionResponseDto1.setAccountId(1);
		
		
		transactionResponseDto2 = new TransactionResponseDTO();
		
		transactionResponseDto2.setTransactionDate(Date.valueOf(LocalDate.now()));
		transactionResponseDto2.setTransactionType("CREDIT");
		transactionResponseDto2.setAmount(1000);
		transactionResponseDto2.setTransactionNumber("1DQTSGSTST653");
		transactionResponseDto2.setAccountId(2);
		
		transactionResponseDTOList.add(transactionResponseDto1);
		transactionResponseDTOList.add(transactionResponseDto2);
		
		
	}
	
	@Test
	@DisplayName("Save Transaction : Positive")
	public void saveTransactionDataTest_Positive() throws IdNotFoundException, InsufficientBalanceException
	{
		when(transactionService.save(transactionRequestDto1, 1)).thenReturn(transactionResponseDTOList);
		
		
		ResponseEntity<List<TransactionResponseDTO>> result = transactionController.MoneyTransfer(transactionRequestDto1, 1);
		
		assertNotNull(result);
		assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Display All Transactions : Positive")
	public void displayAllTransactionsDataTest_Positive()
	{
		when(transactionService.getAllTransaction()).thenReturn(transactionResponseDTOList);
		
		
		ResponseEntity<List<TransactionResponseDTO>> result = transactionController.getAllTransaction();
		
		assertNotNull(result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}
