package com.hcl.consumerbank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.consumerbank.dto.AccountRequestDTO;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.repository.ICustomerRepository;
import com.hcl.consumerbank.service.IAccountService;


@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {

	@Mock
	IAccountService accountService;
	
	
	@InjectMocks
	AccountController accountController;
	
	AccountRequestDTO accountRequestDto1;
	AccountRequestDTO accountRequestDto2;
	
	AccountResponseDTO accountResponseDto1;
	AccountResponseDTO accountResponseDto2;
	
	
	List<AccountResponseDTO> accountResponseDTOList = new ArrayList<>();

	
	
	@BeforeEach
	public void setUp()
	{
		accountRequestDto1 = new AccountRequestDTO();
		
		accountRequestDto1.setAccountNumber(192929L);
		accountRequestDto1.setAccountType("SB");
		accountRequestDto1.setBalance(1000);
		accountRequestDto1.setCustomerId(1);
		
		accountResponseDto1 = new AccountResponseDTO();
		
		BeanUtils.copyProperties(accountRequestDto1, accountResponseDto1);
		
		accountResponseDto2 = new AccountResponseDTO();
		
		accountRequestDto1.setAccountNumber(12329L);
		accountRequestDto1.setAccountType("LA");
		accountRequestDto1.setBalance(1500);
		accountRequestDto1.setCustomerId(12);
		
		accountResponseDTOList.add(accountResponseDto1);
		accountResponseDTOList.add(accountResponseDto2);
		
		
	}
	
	@Test
	@DisplayName("Save Account Data : Positive")
	public void saveAccountDataTest_Positive() throws IdNotFoundException
	{
		
		when(accountService.save(accountRequestDto1)).thenReturn(true);
		
		ResponseEntity<String> result = accountController.saveAccountData(accountRequestDto1);
		
		assertEquals("Account created successfully", result.getBody());
		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
	}
	
	
	@Test
	@DisplayName("Save Account Data : Negative")
	public void saveAccountDataTest_Negative() throws IdNotFoundException
	{
		
		
		when(accountService.save(accountRequestDto1)).thenReturn(false);
		
		
		ResponseEntity<String> result = accountController.saveAccountData(accountRequestDto1);
		
		assertEquals("Account not created", result.getBody());
		assertEquals(HttpStatus.NOT_ACCEPTABLE, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Display Customer Data : Positive")
	public void displayCustomerDataTest_Positive()
	{
		when(accountService.getAllAccountsData()).thenReturn(accountResponseDTOList);
		
		
		ResponseEntity<List<AccountResponseDTO>> result = accountController.getAllAccountsData();
		
		assertNotNull(accountResponseDTOList);
		assertEquals(2, accountResponseDTOList.size());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	
	@Test
	@DisplayName("Delete Account Data By ID : Positive")
	public void deleteAccountByIdTest_Positive() throws IdNotFoundException
	{
		when(accountService.closeAccountById(1)).thenReturn("Successfully Deleted");
		
		ResponseEntity<String> result = accountController.closeAccount(1);
		
		assertEquals("Successfully Deleted" , result.getBody());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
}
