package com.hcl.consumerbank.exception;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class ConsumerBankExceptionHandler {
	
	@ExceptionHandler(IdNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleIdException(HttpServletRequest request, Exception ex){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
		errorResponse.setDateTime(LocalDateTime.now());
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.NOT_FOUND);	
	}
	
	
	@ExceptionHandler(InsufficientBalanceException.class)
	public ResponseEntity<ErrorResponse> handleBalanceException(HttpServletRequest request, Exception ex){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
		errorResponse.setDateTime(LocalDateTime.now());
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.NOT_FOUND);	
	}

	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationErrorResponse> handleException(MethodArgumentNotValidException ex){
		
		List<FieldError> errors = ex.getFieldErrors();
		ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
		validationErrorResponse.setDateTime(LocalDateTime.now());
		validationErrorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
		validationErrorResponse.setMessage("Input Data has errors..");
		
		for(FieldError fieldError : errors)
		{
			validationErrorResponse.getErrors().put(fieldError.getField(), fieldError.getDefaultMessage());
		}
		
		return new ResponseEntity<ValidationErrorResponse>(validationErrorResponse, HttpStatus.BAD_REQUEST);
		
	}
	
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<ValidationErrorResponse> handleException(MethodArgumentTypeMismatchException ex){
		
		ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
		Map<String, String> errors = new HashMap<>();
		validationErrorResponse.setDateTime(LocalDateTime.now());
		validationErrorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
		validationErrorResponse.setMessage("Input Data has errors..");
		errors.put("field", "Invalid date format.! Please enter the date in the correct format : YYYY-MM-DD.");
		validationErrorResponse.setErrors(errors);
		
		return new ResponseEntity<ValidationErrorResponse>(validationErrorResponse, HttpStatus.BAD_REQUEST);
		
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ValidationErrorResponse> handleException(ConstraintViolationException ex){
		
		ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
		validationErrorResponse.setDateTime(LocalDateTime.now());
		validationErrorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
		validationErrorResponse.setMessage("Input Data has errors..");
		
		ex.getConstraintViolations().forEach(error -> {
			validationErrorResponse.getErrors().put("field", error.getMessage());
		});
		
		return new ResponseEntity<ValidationErrorResponse>(validationErrorResponse, HttpStatus.BAD_REQUEST);
		
	}

}
