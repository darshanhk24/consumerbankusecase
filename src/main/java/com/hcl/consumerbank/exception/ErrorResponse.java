package com.hcl.consumerbank.exception;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ErrorResponse {

	private String message;
	private int errorCode;
	private LocalDateTime dateTime;
}
