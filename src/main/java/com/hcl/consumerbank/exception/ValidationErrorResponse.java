package com.hcl.consumerbank.exception;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class ValidationErrorResponse extends ErrorResponse{

	private Map<String, String> errors = new HashMap<>();
}
