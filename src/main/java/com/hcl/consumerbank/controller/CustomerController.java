package com.hcl.consumerbank.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.consumerbank.dto.CustomerRequestDTO;
import com.hcl.consumerbank.dto.CustomerResponse;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.service.ICustomerService;
import com.hcl.consumerbank.utility.GlobalUtility;

//Customer Controller

@RestController
@Validated
public class CustomerController {
	
	private Logger logger = GlobalUtility.getLogger(CustomerController.class);
	
	@Autowired
	ICustomerService customerService;
	
	//To Save Customer Data

	@PostMapping(value = "/customers")
	public ResponseEntity<String> saveCustomerData(@Valid @RequestBody CustomerRequestDTO customerRequestDto)
	{
		String methodName = "saveCuatomerData";
		logger.info(methodName + " called");
		
		boolean response = customerService.save(customerRequestDto);
		if(response)
			return new ResponseEntity<>("Customer saved successfully" , HttpStatus.ACCEPTED);
		else
			return new ResponseEntity<>("Customer not saved" , HttpStatus.NOT_ACCEPTABLE);
	}
	
	// To get all Customer Data
	
	@GetMapping(value = "/customer")
	public ResponseEntity<List<CustomerResponseDTO>> getAllCustomerData()
	{
		String methodName = "getAllCuatomerData";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(customerService.getAllCustomers() , HttpStatus.OK);
	}
	
	//To get customer details by customer name
	
	@GetMapping(value = "/customers")
	public ResponseEntity<List<CustomerResponse>> getCustomerByName(@NotEmpty(message = "customer name should not be empty")@RequestParam("customerName") String name)
	{
		String methodName = "getCustomerByName";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(customerService.getCustomerByName(name) , HttpStatus.OK);
	}
	
	 //To get customer details by customer Id
	
	@GetMapping(value = "/customers/{customerId}")
	public ResponseEntity<CustomerResponseDTO> getCustomerById(@PathVariable int customerId) throws IdNotFoundException
	{
		String methodName = "getCustomerById";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(customerService.getCustomerById(customerId) , HttpStatus.OK);
	}
	
	//To delete particular customer
	
	@DeleteMapping(value = "/customers/{customerId}")
	public ResponseEntity<String> deleteCustomerById(@PathVariable int customerId) throws IdNotFoundException
	{
		String methodName = "deleteCustomerById";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(customerService.deleteCustomerById(customerId) , HttpStatus.OK);
	}
	
	// To update particular customer
	
	@PutMapping(value = "/customers/{customerId}")
	public ResponseEntity<String> updateCustomerById(@Valid @RequestBody CustomerRequestDTO customerRequestDto,@PathVariable int customerId) throws IdNotFoundException
	{
		String methodName = "updateCustomerById";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(customerService.updateCustomer(customerRequestDto, customerId), HttpStatus.OK);
	}
	
	// To get Transaction details of particular customer by month
	
	@GetMapping(value = "/customers/transactions/{customerId}/{month}/{year}")
	public ResponseEntity<List<TransactionResponseDTO>> getCustomerTransactionByMonth(@PathVariable int customerId, @Min(value = 1, message = "Enter valid month (1-12)" )@Max(value = 12, message = "Enter valid month (1-12)")@PathVariable int month , @Min(value = 1000) @Max(value = 2021)@PathVariable int year) throws IdNotFoundException
	{
		String methodName = "getCustomerTransactionByMonth";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(customerService.getCustomerTransactionByMonth(customerId, month, year) , HttpStatus.OK);
	}
}
