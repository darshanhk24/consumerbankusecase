package com.hcl.consumerbank.controller;

import java.util.List;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.consumerbank.dto.AccountRequestDTO;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.service.IAccountService;
import com.hcl.consumerbank.utility.GlobalUtility;

import org.springframework.web.bind.annotation.RestController;

//Account Controller

@RestController
public class AccountController {
	
	private Logger logger = GlobalUtility.getLogger(AccountController.class);

	@Autowired
	IAccountService accountService;
	
	// To save account details
	
	@PostMapping(value = "/accounts")
	public ResponseEntity<String> saveAccountData(@Valid @RequestBody AccountRequestDTO accountRequestDto) throws IdNotFoundException
	{
		String methodName = "saveAccountData";
		logger.info(methodName + " called");
		
		boolean response = accountService.save(accountRequestDto);
		if(response)
			return new ResponseEntity<>("Account created successfully" , HttpStatus.ACCEPTED);
		else
			return new ResponseEntity<>("Account not created" , HttpStatus.NOT_ACCEPTABLE);
	}
	
	// To get all account details
	
	@GetMapping(value = "/accounts")
	public ResponseEntity<List<AccountResponseDTO>> getAllAccountsData()
	{
		String methodName = "getAllAccountsData";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(accountService.getAllAccountsData() , HttpStatus.OK);
	}
	
	//To close account of particular customer
	
	@DeleteMapping(value = "/accounts/{accountId}")
	public ResponseEntity<String> closeAccount(@PathVariable int accountId) throws IdNotFoundException
	{
		String methodName = "closeAccount";
		logger.info(methodName + " called");
		return new ResponseEntity<>(accountService.closeAccountById(accountId) , HttpStatus.OK);
	}
}
