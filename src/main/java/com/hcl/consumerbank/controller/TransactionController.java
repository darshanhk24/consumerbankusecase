package com.hcl.consumerbank.controller;

import java.sql.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.dto.CustomerRequestDTO;
import com.hcl.consumerbank.dto.TransactionRequestDTO;
import com.hcl.consumerbank.dto.TransactionResponse;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.exception.InsufficientBalanceException;
import com.hcl.consumerbank.service.ITransactionService;
import com.hcl.consumerbank.utility.GlobalUtility;

//Transaction Controller

@RestController
public class TransactionController {
	
	private Logger logger = GlobalUtility.getLogger(TransactionController.class);
	
	@Autowired
	ITransactionService transactionService;
	
	// To transfer amount from one account to another account

	@PostMapping(value = "/transactions/{senderAccountId}")
	public ResponseEntity<List<TransactionResponseDTO>> MoneyTransfer(@Valid @RequestBody TransactionRequestDTO transactionRequestDto, @PathVariable("senderAccountId") int senderAccountId ) throws IdNotFoundException, InsufficientBalanceException
	{
		String methodName = "MoneyTransfer";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(transactionService.save(transactionRequestDto, senderAccountId) , HttpStatus.CREATED);
	}
	
	// To get all transaction details
	
	@GetMapping(value = "/transactions")
	public ResponseEntity<List<TransactionResponseDTO>> getAllTransaction()
	{
		String methodName = "getAllTransaction";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(transactionService.getAllTransaction() , HttpStatus.OK);
	}
	
	// To get transaction details between two transaction dates
	
	@GetMapping(value = "/transactions/{fromDate}/{toDate}")
	public ResponseEntity<String> getTransactionsAmountDetailsBetweenDates(@Valid @PathVariable Date fromDate, @Valid @PathVariable Date toDate) throws IdNotFoundException
	{
		String methodName = "getTransactionsByAccountId";
		logger.info(methodName + " called");
		
		return new ResponseEntity<>(transactionService.getTransactionsAmountDetailsBetweenDates(fromDate , toDate), HttpStatus.OK);
	}
}
