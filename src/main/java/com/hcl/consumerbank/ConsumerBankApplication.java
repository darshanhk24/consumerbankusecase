package com.hcl.consumerbank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
public class ConsumerBankApplication {

	public static void main(String[] args) {
		
		Logger logger = LoggerFactory.getLogger(ConsumerBankApplication.class);
		logger.info("Application Starting");
		SpringApplication.run(ConsumerBankApplication.class, args);
	}

}
