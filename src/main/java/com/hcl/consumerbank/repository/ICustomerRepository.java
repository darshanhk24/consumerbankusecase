package com.hcl.consumerbank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.consumerbank.dto.CustomerResponse;
import com.hcl.consumerbank.entity.Customer;

@Repository
public interface ICustomerRepository extends JpaRepository<Customer, Integer> {
	
	public List<CustomerResponse> findByCustomerNameContaining(String customerName);

}
