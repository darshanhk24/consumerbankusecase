package com.hcl.consumerbank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.consumerbank.dto.AccountResponse;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Customer;

@Repository
public interface IAccountRepository extends JpaRepository<Account , Integer>{

	List<AccountResponse> findByCustomerLike(Customer customer);
}
