package com.hcl.consumerbank.repository;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.consumerbank.dto.TransactionResponse;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Transaction;

@Repository
public interface ITransactionRepository extends JpaRepository<Transaction, Integer>{
	
	List<TransactionResponse> findByTransactionDateBetween(Date transactionFromDate, Date transactionToDate); 
	
	
	// query to get transactions of particular account by transaction date and month
	
	@Query("SELECT t FROM com.hcl.consumerbank.entity.Transaction t WHERE (t.account) = :account AND MONTH(t.transactionDate) = :month AND YEAR(t.transactionDate) = :year")
	List<Transaction> findByAccountWhereMonthAndYear(Account account, Integer month, Integer year);

}
