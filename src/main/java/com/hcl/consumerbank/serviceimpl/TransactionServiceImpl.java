package com.hcl.consumerbank.serviceimpl;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.consumerbank.controller.AccountController;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.dto.TransactionRequestDTO;
import com.hcl.consumerbank.dto.TransactionResponse;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Transaction;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.exception.InsufficientBalanceException;
import com.hcl.consumerbank.repository.IAccountRepository;
import com.hcl.consumerbank.repository.ITransactionRepository;
import com.hcl.consumerbank.service.ITransactionService;
import com.hcl.consumerbank.utility.GlobalUtility;


@Service
public class TransactionServiceImpl implements ITransactionService {
	
	private Logger logger = GlobalUtility.getLogger(TransactionServiceImpl.class);
	
	@Autowired
	IAccountRepository accountRepository;
	
	@Autowired
	ITransactionRepository transactionRepository;
	
	double totalCreditAmount = 0;
	double totalDebitAmount = 0;

	@Override
	public List<TransactionResponseDTO> save(@Valid TransactionRequestDTO transactionRequestDto, int senderAccountId) throws IdNotFoundException, InsufficientBalanceException {
		// TODO Auto-generated method stub
		
		String methodName = "save";
		logger.info(methodName + " called");
		
		Transaction transaction1 = new Transaction();
		TransactionResponseDTO transactionResponseDto1 = new TransactionResponseDTO();
		List<TransactionResponseDTO> transactionResponseList = new ArrayList<TransactionResponseDTO>();
		
		// to generate random transaction number
		
		String TransactionNumber = RandomStringUtils.randomAlphanumeric(10).toUpperCase();
		
		
		
		Account account1 = accountRepository.findById(senderAccountId).orElseThrow(()-> new IdNotFoundException("Account Id Not Exist.!"));
		if(account1.getBalance() < transactionRequestDto.getAmount())
			throw new InsufficientBalanceException("Insufficient funds.! ");
		else
		{
			account1.setBalance(account1.getBalance() - transactionRequestDto.getAmount());
			transaction1.setTransactionDate(Date.valueOf(LocalDate.now()));
			transaction1.setTransactionType("DEBIT");
			transaction1.setAmount(transactionRequestDto.getAmount());
			transaction1.setTransactionNumber(TransactionNumber);
			transaction1.setAccount(account1);
			transactionRepository.save(transaction1);
			BeanUtils.copyProperties(transaction1, transactionResponseDto1);
			transactionResponseDto1.setAccountId(account1.getAccountId());
			transactionResponseList.add(transactionResponseDto1);
		}
			
		
		
		Transaction transaction2 = new Transaction();
		TransactionResponseDTO transactionResponseDto2 = new TransactionResponseDTO();
		Account account2 = accountRepository.findById(transactionRequestDto.getAccountId()).orElseThrow(()-> new IdNotFoundException("Account Id Not Exist.!"));
		account2.setBalance(account2.getBalance() + transactionRequestDto.getAmount());
		transaction2.setTransactionDate(Date.valueOf(LocalDate.now()));
		transaction2.setTransactionType("CREDIT");
		transaction2.setAmount(transactionRequestDto.getAmount());
		transaction2.setTransactionNumber(TransactionNumber);
		transaction2.setAccount(account2);
		transactionRepository.save(transaction2);
		BeanUtils.copyProperties(transaction2, transactionResponseDto2);
		transactionResponseDto2.setAccountId(account2.getAccountId());
		transactionResponseList.add(transactionResponseDto2);
		
		return transactionResponseList;
	}

	@Override
	public List<TransactionResponseDTO> getAllTransaction() {
		// TODO Auto-generated method stub
		
		String methodName = "getAllTransaction";
		logger.info(methodName + " called");
		
		List<TransactionResponseDTO> transactionResponseList = new ArrayList<TransactionResponseDTO>();
		
		transactionRepository.findAll().forEach(transaction -> {
			TransactionResponseDTO transactionResponseDto= new TransactionResponseDTO();
			BeanUtils.copyProperties(transaction, transactionResponseDto);
			transactionResponseDto.setAccountId(transaction.getAccount().getAccountId());
			transactionResponseList.add(transactionResponseDto);
		});
		return transactionResponseList;
	}

	@Override
	public List<TransactionResponseDTO> getTransactionsByAccountId(int accountId) throws IdNotFoundException {
		// TODO Auto-generated method stub
		String methodName = "getTransactionsByAccountId";
		logger.info(methodName + " called");
		
		Account account = accountRepository.findById(accountId).orElseThrow(()-> new IdNotFoundException("Account Id Not Exist.!"));
		List<TransactionResponseDTO> transactionResponseList  = new ArrayList<TransactionResponseDTO>();
		List<Transaction> transactionList = account.getTransaction();
		
		transactionList.forEach(transaction -> {
			TransactionResponseDTO transactionResponseDto = new TransactionResponseDTO();
			BeanUtils.copyProperties(transaction, transactionResponseDto);
			transactionResponseDto.setAccountId(transaction.getAccount().getAccountId());
			transactionResponseList.add(transactionResponseDto);
		});
		return transactionResponseList;
	}

	@Override
	public String getTransactionsAmountDetailsBetweenDates(Date fromDate, Date toDate) {
		// TODO Auto-generated method stub
		
		List<TransactionResponse> transactionResponseList = transactionRepository.findByTransactionDateBetween(fromDate, toDate);
		
		transactionResponseList.forEach(transaction ->{
			if(transaction.getTransactionType().equals("CREDIT"))
				totalCreditAmount += transaction.getAmount();
			else
				totalDebitAmount += transaction.getAmount();
		});
		
		if(transactionResponseList.isEmpty())
			return "No transactions found from "+fromDate+ " to "+toDate;
		else	
			return "Transaction Amount details from "+fromDate+ " to "+toDate+ "\nTotal Credit "+totalCreditAmount+"\nTotal Debit "+totalDebitAmount;
	}

}
