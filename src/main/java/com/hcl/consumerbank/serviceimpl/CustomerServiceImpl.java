package com.hcl.consumerbank.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.consumerbank.controller.AccountController;
import com.hcl.consumerbank.dto.AccountResponse;
import com.hcl.consumerbank.dto.CustomerRequestDTO;
import com.hcl.consumerbank.dto.CustomerResponse;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.entity.Transaction;
import com.hcl.consumerbank.repository.IAccountRepository;
import com.hcl.consumerbank.repository.ICustomerRepository;
import com.hcl.consumerbank.repository.ITransactionRepository;
import com.hcl.consumerbank.service.ICustomerService;
import com.hcl.consumerbank.utility.GlobalUtility;
import com.hcl.consumerbank.exception.IdNotFoundException;

@Service
public class CustomerServiceImpl implements ICustomerService {
	
	private Logger logger = GlobalUtility.getLogger(CustomerServiceImpl.class);

	@Autowired
	ICustomerRepository customerRepository;
	
	@Autowired
	IAccountRepository accountRepository;
	
	@Autowired
	ITransactionRepository transactionRepository;
	
	
	@Override
	public boolean save(CustomerRequestDTO customerRequestDto) {
		// TODO Auto-generated method stub
		String methodName = "save";
		logger.info(methodName + "called");
		
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRequestDto, customer);
		Customer savedCustomer = customerRepository.save(customer);
		if(savedCustomer != null)
			return true;
		else
			return false;
			
		
	}


	@Override
	public List<CustomerResponseDTO> getAllCustomers() {
		// TODO Auto-generated method stub
		String methodName = "getAllCustomers";
		logger.info(methodName + " called");
		
		List<CustomerResponseDTO> customerResponseList = new ArrayList<CustomerResponseDTO>();
		
		customerRepository.findAll().forEach(customer ->{
			CustomerResponseDTO customerResponseDto = new CustomerResponseDTO();
			BeanUtils.copyProperties(customer, customerResponseDto);
			customerResponseList.add(customerResponseDto);
		});
		
		return customerResponseList;
	}


	@Override
	public List<CustomerResponse> getCustomerByName(String name) {
		// TODO Auto-generated method stub
		String methodName = "getCustomerByName";
		logger.info(methodName + " called");
		
		
		return customerRepository.findByCustomerNameContaining(name);
	}


	@Override
	public CustomerResponseDTO getCustomerById(int customerId) throws IdNotFoundException {
		// TODO Auto-generated method stub
		String methodName = "getCustomerById";
		logger.info(methodName + " called");
		
		Customer customer = customerRepository.findById(customerId).orElseThrow(()-> new IdNotFoundException("Customer Id Not Exist.!"));
		CustomerResponseDTO customerResponseDto = new CustomerResponseDTO();
		BeanUtils.copyProperties(customer, customerResponseDto);
		return customerResponseDto;
	}


	@Override
	public String deleteCustomerById(int customerId) throws IdNotFoundException {
		// TODO Auto-generated method stub
		String methodName = "deleteCustomerById";
		logger.info(methodName + " called");
		
		Customer customer = customerRepository.findById(customerId).orElseThrow(()-> new IdNotFoundException("Customer Id Not Exist.!"));
		customerRepository.delete(customer);
		return "Successfully Deleted";
	}


	@Override
	public String updateCustomer(CustomerRequestDTO customerRequestDto, int customerId) throws IdNotFoundException {
		// TODO Auto-generated method stub
		String methodName = "updateCustomer";
		logger.info(methodName + " called");
		
		Customer customer = customerRepository.findById(customerId).orElseThrow(()-> new IdNotFoundException("Customer Id Not Exist.!"));
		BeanUtils.copyProperties(customerRequestDto, customer);
		customerRepository.save(customer);
		return "Customer updated successfully";
	}


	@Override
	public List<TransactionResponseDTO> getCustomerTransactionByMonth(int customerId, int month, int year) throws IdNotFoundException {
		// TODO Auto-generated method stub
		customerRepository.findById(customerId).orElseThrow(()-> new IdNotFoundException("Customer Id Not Exist.!"));
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		
		List<AccountResponse> accountList = accountRepository.findByCustomerLike(customer);
		
		if(accountList.isEmpty())
			throw new IdNotFoundException("Account Not Found for this customer Id.!");
		
		List<Transaction> transactionList = new ArrayList<>();
		accountList.forEach(account ->{
			Account acc = new Account();
			acc.setAccountId(account.getAccountId());
			transactionList.addAll(transactionRepository.findByAccountWhereMonthAndYear(acc, month, year));
			
		});
		
		List<TransactionResponseDTO> transactionResponseList = new ArrayList<>();
		transactionList.forEach(transaction->{
			TransactionResponseDTO transactionResponseDto = new TransactionResponseDTO();
			BeanUtils.copyProperties(transaction, transactionResponseDto);
			transactionResponseDto.setAccountId(transaction.getAccount().getAccountId());
			transactionResponseList.add(transactionResponseDto);
		});
		
		return transactionResponseList;
	}

}
