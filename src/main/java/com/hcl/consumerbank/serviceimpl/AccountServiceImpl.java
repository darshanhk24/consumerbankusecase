package com.hcl.consumerbank.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.consumerbank.controller.AccountController;
import com.hcl.consumerbank.dto.AccountRequestDTO;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.entity.Account;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.repository.IAccountRepository;
import com.hcl.consumerbank.repository.ICustomerRepository;
import com.hcl.consumerbank.service.IAccountService;
import com.hcl.consumerbank.utility.GlobalUtility;
import com.hcl.consumerbank.exception.IdNotFoundException;

@Service
public class AccountServiceImpl implements IAccountService {
	
	private Logger logger = GlobalUtility.getLogger(AccountServiceImpl.class);

	@Autowired
	IAccountRepository accountRepository;
	
	@Autowired
	ICustomerRepository customerRepository;
	
	@Override
	public boolean save(AccountRequestDTO accountRequestDto) throws IdNotFoundException {
		// TODO Auto-generated method stub
		
		String methodName = "save";
		logger.info(methodName + " called");
		
		Account account = new Account();
		Customer customer = customerRepository.findById(accountRequestDto.getCustomerId()).orElseThrow(()-> new IdNotFoundException("Customer Id Not Exist.!")); 
		account.setCustomer(customer);
		BeanUtils.copyProperties(accountRequestDto, account);
		Account savedAccount = accountRepository.save(account);
		
		if(savedAccount != null)
			return true;
		else
			return false;
		
	}

	@Override
	public List<AccountResponseDTO> getAllAccountsData() {
		// TODO Auto-generated method stub
		
		String methodName = "getAllAccountsData";
		logger.info(methodName + " called");
		
		List<AccountResponseDTO> accountResponseList = new ArrayList<AccountResponseDTO>();
		
		accountRepository.findAll().forEach(acc -> {
			AccountResponseDTO accountResponseDto = new AccountResponseDTO();
			BeanUtils.copyProperties(acc, accountResponseDto);
			accountResponseDto.setCustomerId(acc.getCustomer().getCustomerId());
			accountResponseList.add(accountResponseDto);
		});;
		
//		for(Account account : accountList)
//		{
//			AccountResponseDTO accountResponseDto = new AccountResponseDTO();
//			accountResponseDto.setCustomerId(account.getCustomer().getCustomerId());
//			BeanUtils.copyProperties(account, accountResponseDto);
//			accountResponseList.add(accountResponseDto);
//		}
		return accountResponseList;
	}

	@Override
	public String closeAccountById(int accountId) throws IdNotFoundException {
		// TODO Auto-generated method stub
		String methodName = "closeAccountById";
		logger.info(methodName + " called");
		
		Account account = accountRepository.findById(accountId).orElseThrow(()-> new IdNotFoundException("Account Id Not Exist.!"));
		accountRepository.delete(account);
		return "Successfully Deleted";
	}

}
