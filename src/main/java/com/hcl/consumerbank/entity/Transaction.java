package com.hcl.consumerbank.entity;

import java.time.LocalDateTime;
import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer transactionId;
	private String transactionNumber;
	private double amount;
	private String transactionType;
	private Date transactionDate;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account account;

}
