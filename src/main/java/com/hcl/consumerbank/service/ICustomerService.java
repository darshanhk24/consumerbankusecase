package com.hcl.consumerbank.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.consumerbank.dto.CustomerRequestDTO;
import com.hcl.consumerbank.dto.CustomerResponse;
import com.hcl.consumerbank.dto.CustomerResponseDTO;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.entity.Customer;
import com.hcl.consumerbank.exception.IdNotFoundException;


@Service
public interface ICustomerService {

	boolean save(CustomerRequestDTO customerRequestDto);

	List<CustomerResponseDTO> getAllCustomers();

	List<CustomerResponse> getCustomerByName(String name);

	CustomerResponseDTO getCustomerById(int customerId)  throws IdNotFoundException;
	
	String deleteCustomerById(int accountId) throws IdNotFoundException;

	String updateCustomer(CustomerRequestDTO customerRequestDto, int customerId) throws IdNotFoundException;

	List<TransactionResponseDTO> getCustomerTransactionByMonth(int customerId, int month, int year) throws IdNotFoundException;

}
