package com.hcl.consumerbank.service;

import java.sql.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.hcl.consumerbank.dto.TransactionRequestDTO;
import com.hcl.consumerbank.dto.TransactionResponse;
import com.hcl.consumerbank.dto.TransactionResponseDTO;
import com.hcl.consumerbank.exception.IdNotFoundException;
import com.hcl.consumerbank.exception.InsufficientBalanceException;

@Service
public interface ITransactionService {

	List<TransactionResponseDTO> save(@Valid TransactionRequestDTO transactionRequestDto, int senderAccountId) throws IdNotFoundException, InsufficientBalanceException;

	List<TransactionResponseDTO> getAllTransaction();

	List<TransactionResponseDTO> getTransactionsByAccountId(int accountId) throws IdNotFoundException;

	String getTransactionsAmountDetailsBetweenDates(Date fromDate, Date toDate);

}
