package com.hcl.consumerbank.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.consumerbank.dto.AccountRequestDTO;
import com.hcl.consumerbank.dto.AccountResponseDTO;
import com.hcl.consumerbank.exception.IdNotFoundException;

@Service
public interface IAccountService {

	boolean save(AccountRequestDTO accountRequestDto) throws IdNotFoundException;

	List<AccountResponseDTO> getAllAccountsData();

	String closeAccountById(int accountId) throws IdNotFoundException;

}
