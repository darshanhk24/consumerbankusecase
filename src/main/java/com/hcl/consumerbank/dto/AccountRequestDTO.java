package com.hcl.consumerbank.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AccountRequestDTO {

	@NotNull(message = "Account number cannot be null")
	private Long accountNumber;
	
	@Min(message = "Amount cannot be less than 1000", value = 1000)
	private double balance;
	
	private Integer customerId;
	
	@NotEmpty(message = "Account type cannot be empty")
	private String accountType;
}
