package com.hcl.consumerbank.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcl.consumerbank.entity.Customer;

// Class based projection

public class AccountResponse {

	private Integer accountId;
	private Long accountNumber;
	private double balance;
	private String accountType;
	@JsonIgnore
	private Customer customer;
	private Integer customerId;
	
	public AccountResponse(Integer accountId, Long accountNumber, double balance, String accountType,
			Customer customer) {
		super();
		this.accountId = accountId;
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.accountType = accountType;
		this.customer = customer;
		this.customerId = getCustomer().getCustomerId();
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	
}
