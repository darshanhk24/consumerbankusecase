package com.hcl.consumerbank.dto;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class TransactionRequestDTO {

	@NotNull(message = "Amount account be empty")
	@Min(value = 100, message = "Minimum transaction is 100")
	private double amount;
	
	private Integer accountId;
	
	
}
