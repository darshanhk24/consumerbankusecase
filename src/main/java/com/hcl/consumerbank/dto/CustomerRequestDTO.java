package com.hcl.consumerbank.dto;

import javax.validation.constraints.NotEmpty;


import lombok.Data;

@Data
public class CustomerRequestDTO {

	@NotEmpty(message = "Customer name cannot be empty")
	private String customerName;
	
	@NotEmpty(message = "Customer phone number cannot be empty")
	private String phoneNo;
	
	@NotEmpty(message = "Customer message cannot be empty")
	private String address;
}
