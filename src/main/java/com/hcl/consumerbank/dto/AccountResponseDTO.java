package com.hcl.consumerbank.dto;



import lombok.Data;

@Data
public class AccountResponseDTO {

	private Long accountNumber;
	private double balance;
	private int customerId;
}
