package com.hcl.consumerbank.dto;

import java.time.LocalDateTime;
import java.sql.Date;

import lombok.Data;

@Data
public class TransactionResponseDTO {

	private Integer transactionId;
	private String transactionNumber;
	private double amount;
	private String transactionType;
	private Integer accountId;
	private Date transactionDate;
}
