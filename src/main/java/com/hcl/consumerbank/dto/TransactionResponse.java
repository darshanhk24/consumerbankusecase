package com.hcl.consumerbank.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcl.consumerbank.entity.Account;

// Class based projection

public class TransactionResponse {

	private Integer transactionId;
	private String transactionNumber;
	private double amount;
	private String transactionType;
	private Date transactionDate;
	@JsonIgnore
	private Account account;
	private Integer accountId;
	
	public TransactionResponse(Integer transactionId, String transactionNumber, double amount, String transactionType,
			Date transactionDate, Account account) {
		super();
		this.transactionId = transactionId;
		this.transactionNumber = transactionNumber;
		this.amount = amount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.account = account;
		this.accountId = getAccount().getAccountId();
	}
	
	public Integer getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getTransactionNumber() {
		return transactionNumber;
	}
	
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	public Date getTransactionDate() {
		return transactionDate;
	}
	
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Integer getAccountId() {
		return accountId;
	}
	

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	
}
