package com.hcl.consumerbank.dto;

// Interface based projection

public interface CustomerResponse {
	
	String getCustomerName();
	
	void setCustomerName(String customerName);
	
	String getPhoneNo();
	
	void setPhoneNo(String phoneNo);
	
	String getAddress();
	
	void setAddress(String address);

}
