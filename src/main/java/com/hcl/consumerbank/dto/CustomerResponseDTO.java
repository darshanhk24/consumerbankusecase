package com.hcl.consumerbank.dto;

import lombok.Data;

@Data
public class CustomerResponseDTO {

	private Integer customerId;
	private String customerName;
	private String phoneNo;
	private String address;
}
